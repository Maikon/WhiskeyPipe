from flask import Flask
from recommender import Recommender

app = Flask(__name__)

r = Recommender()


@app.route('/<string:whiskey>')
def show_user_profile(whiskey, recommender=r):
    print("Recommending whiskey for %s" % whiskey)
    return recommender.reccomend(whiskey).to_json(orient="records")


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80)
