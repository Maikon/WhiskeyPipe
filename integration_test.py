import json
import urllib.request
from flask import Flask
from flask_testing import LiveServerTestCase

import main


# Testing with LiveServer
class IntegrationTest(LiveServerTestCase):
    # if the create_app is not implemented NotImplementedError will be raised
    def create_app(self):
        app = main.app
        app.config['TESTING'] = True
        # Default port is 5000
        app.config['LIVESERVER_PORT'] = 0
        # Default timeout is 5 seconds
        app.config['LIVESERVER_TIMEOUT'] = 5
        return app

    def test_flask_application_is_up_and_running(self):
        response = urllib.request.urlopen(self.get_server_url() + "/Laphroig")
        assert response.code == 200
        data = json.load(response)
        assert len(data) > 0
        error = True
        for d in data:
            if d == "Lagavulin":
                error = False
        assert not error
