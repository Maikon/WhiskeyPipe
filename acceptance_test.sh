#!/usr/bin/env sh

set -x

docker run -d --name whiskeypipe ${IMAGE_TAG}

counter=0
OUTPUT=$(docker run -i --link whiskeypipe alpine:3.6 wget -qO- http://whiskeypipe/Laphroig)
while [[ -z "${OUTPUT}" && "${counter}" -lt 10 ]]; do
    OUTPUT=$(docker run -i --link whiskeypipe alpine:3.6 wget -qO- http://whiskeypipe/Laphroig)
    sleep 1
    counter=$((counter+1))
done


ERROR=1
$(echo "${OUTPUT}" | grep -q "Lagavulin") && ERROR=0 || echo ERROR=1

docker rm -f whiskeypipe

exit ${ERROR}