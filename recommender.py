import pandas as pd
import math
import numpy as np


class Recommender:

    def __init__(self):
        self.__X = pd.read_pickle("X.pkl")
        self.__y = pd.read_pickle("y.pkl")

    def reccomend(self, whiskey="Laphroig"):
        try:
            testInstance = self.__X.loc[self.__y == whiskey]
            neighbors = self.__getNeighbors(self.__X.as_matrix(), testInstance.as_matrix()[0], 5)
            return self.__y.iloc[neighbors]
        except IndexError:
            return pd.DataFrame([])

    def __euclideanDistance(self, instance1, instance2):
        distance = 0
        for x in range(len(instance1)):
            distance += pow((instance1[x] - instance2[x]), 2)
        return math.sqrt(distance)

    def __getNeighbors(self, trainingSet, instance, k):
        """Return the first k locations of the nearest neighbours to an instance"""
        distances = []
        for x in range(len(trainingSet)):
            dist = self.__euclideanDistance(instance, trainingSet[x])
            distances.append(dist)
        locs = np.argsort(distances)
        return locs[:k]
